<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.sql.*, javax.sql.*, javax.naming.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<title>CSE 135 Cosine Similarity</title>
</head>
<body>
<%	
Connection conn = null; 
Statement stmt = null; 
try
{
	Class.forName("org.postgresql.Driver");
	String url = "jdbc:postgresql://localhost:5432/CSE135";
	String admin = "postgres";
	String password = "postgre";
	conn = DriverManager.getConnection(url, admin, password);
}
catch(Exception e)
{
}

//PreparedStatement pstmt2 = conn.prepareStatement("SELECT p1.product_id, p2.product_id, SUM(p1.price)*SUM(p2.price) FROM orders p1, orders p2 WHERE p1.id<p2.id AND p1.product_id!=p2.product_id GROUP BY p1.product_id , p2.product_id LIMIT 10");
//ResultSet rs = pstmt2.executeQuery();
//PreparedStatement pstmt2 = conn.prepareStatement("SELECT SUM(o.price) AS sumprice, product_id FROM orders o GROUP BY o.product_id ORDER BY sumprice DESC LIMIT 10");
//ResultSet rs = pstmt2.executeQuery();
String table1 = "(SELECT SUM(o.price) AS sumprice, product_id, user_id FROM orders o GROUP BY o.product_id,o.user_id )";
String table2 = "(SELECT SUM(o.price) AS grossprice, product_id FROM orders o GROUP BY o.product_id )";
String temp1 = "CREATE TEMPORARY TABLE IF NOT EXISTS temp1 AS (SELECT SUM(o.price) AS sumprice, product_id, user_id FROM orders o GROUP BY o.product_id,o.user_id )";
String temp2 = "CREATE TEMPORARY TABLE IF NOT EXISTS temp2 AS (SELECT SUM(o.price) AS grossprice, product_id FROM orders o GROUP BY o.product_id )";
PreparedStatement pstmt6 = conn.prepareStatement(temp1);
pstmt6.execute();
pstmt6 = conn.prepareStatement(temp2);
pstmt6.execute(); 
 pstmt6 = conn.prepareStatement("CREATE INDEX temp1_indx1 ON temp1 USING btree (product_id)");
 pstmt6.execute(); 
 pstmt6 = conn.prepareStatement("CREATE INDEX temp1_indx2 ON temp1 USING btree (user_id)");
 pstmt6.execute(); 
 pstmt6 = conn.prepareStatement("CREATE INDEX temp2_indx1 ON temp2 USING btree (product_id)");
 pstmt6.execute();
// PreparedStatement pstmt2 = conn.prepareStatement("SELECT (SUM((t1.sumprice*t2.sumprice)))/(t3.grossprice*t4.grossprice) AS sp, t1.product_id, t2.product_id FROM "+table1+" AS t1,"+table1+"AS t2,"+table2+" AS t3,"+table2+" AS t4 WHERE t1.user_id = t2.user_id AND t1.product_id<t2.product_id AND t1.product_id=t3.product_id AND t2.product_id=t4.product_id GROUP BY t1.product_id,t2.product_id,t1.sumprice,t2.sumprice,t3.grossprice,t4.grossprice ORDER BY sp DESC LIMIT 100");

PreparedStatement pstmt2 = conn.prepareStatement("SELECT (SUM((t1.sumprice*t2.sumprice)))/(t3.grossprice*t4.grossprice) AS sp, t1.product_id, t2.product_id FROM temp1 t1,temp1 t2,temp2 t3,temp2 t4 WHERE t1.user_id = t2.user_id AND t1.product_id<t2.product_id AND t1.product_id=t3.product_id AND t2.product_id=t4.product_id GROUP BY t1.product_id,t2.product_id,t1.sumprice,t2.sumprice,t3.grossprice,t4.grossprice ORDER BY sp DESC LIMIT 100");
ResultSet rs = pstmt2.executeQuery();
while(rs.next()){
	out.println("SumPrice: "+rs.getFloat("sp")+" Prod1: "+rs.getInt(2)+" Prod2: "+rs.getInt(3)+"          ");%><br><%
}
System.out.println("doneSSSS");

%>
</body>
</html>
