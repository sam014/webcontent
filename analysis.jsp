<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.sql.*, javax.sql.*, javax.naming.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<title>CSE 135 Analysis</title>
</head>
<body>
<%	
    Connection conn = null; 
    Statement stmt = null; 
    int rowsLimits = 0; 
    int colLimits = 0; 
    
    String nextProduct = null;
    String nextName = null; 
    double nextK = 0; 
    double nextSum = 0; 
    try
	{
		Class.forName("org.postgresql.Driver");
		String url = "jdbc:postgresql://localhost:5432/CSE135";
		String admin = "postgres";
		String password = "postgre";
		conn = DriverManager.getConnection(url, admin, password);
	}
    catch(Exception e)
	{
	}
    
    if(session.getAttribute("alphabetical") != null)
    {
    	nextProduct = session.getAttribute("alphabetical").toString(); 
    }
    if(session.getAttribute("topk") != null)
    {
    	nextK = Double.parseDouble(session.getAttribute("topk").toString());
    }
    if(session.getAttribute("nextName") != null)
    {
    	nextName = session.getAttribute("nextName").toString(); 
    }
    if(session.getAttribute("nextSum") != null)
    {
    	nextSum = Double.parseDouble(session.getAttribute("nextSum").toString()); 
    }
    
    String rows = request.getParameter("rows");
    String order = request.getParameter("order");
    String filter = request.getParameter("filter"); 
    
    if(rows != null && order != null && filter != null)
    {
    	session.setAttribute("rows", rows);
    	session.setAttribute("order", order);
    	session.setAttribute("filter", filter); 
    }
    
    if(request.getParameter("action") != null && request.getParameter("action").equals("Reset"))
    {
		rows = null;
		order = null; 
		filter = null; 
		
		session.removeAttribute("rows");
		session.removeAttribute("order");
		session.removeAttribute("filter"); 
		session.removeAttribute("alphabetical");
		session.removeAttribute("topk"); 
		session.removeAttribute("nextName"); 
		session.removeAttribute("nextSum"); 
    }
    
    if(session.getAttribute("rows") != null && session.getAttribute("order") != null && session.getAttribute("filter") != null)
    {
    	rows = session.getAttribute("rows").toString();
    	order = session.getAttribute("order").toString();
    	filter = session.getAttribute("filter").toString(); 
    }
    
    System.out.println(rows + " " + order + " " + filter); 
%>
<form action="analysis.jsp" method="post">
<label for="rows">Rows</label>
  	<select name="rows" id="rows" class="form-control">
  	<%
  		if(rows == null || rows.equals("customers"))
  		{
  	%>
		<option value="customers">Customers</option>
		<option value="states">States</option>
	<%
  		}
  		else
  		{
	%>
		<option value="states">States</option>
		<option value="customers">Customers</option>
	<%
  		}
	%>
	    
	</select>
	
<label for="order">Order</label>
  	<select name="order" id="order" class="form-control">
  	<%
  		if(order == null || order.equals("alphabetical"))
  		{
  	%>
		<option value="alphabetical">Alphabetical</option>
		<option value="top-k">Top-K</option>
	<%
  		}
  		else
  		{
	%>
		<option value="top-k">Top-K</option>
		<option value="alphabetical">Alphabetical</option>
	<%
  		}
	%>
	</select>
	
<label for="filter">Sales Filter</label>
  	<select name="filter" id="filter" class="form-control">
		<%
		    String lastCatName = null; 
		    int lastCatId = -1; 
		
			if(filter != null && !filter.equals("all"))
			{
				stmt = conn.createStatement();
				String sql = "SELECT name, id FROM categories WHERE id = " + filter;
				ResultSet result = stmt.executeQuery(sql);
				
				if(result.next())
				{
					lastCatName = result.getString("name");
					lastCatId = result.getInt("id"); 
		%>
					<option value="<%=lastCatId%>"> <%out.print(lastCatName);%> </option>
		<%					
				}
			}
		%>
			<option value="all"> All </option>
		<%
			stmt = conn.createStatement();
			String sql; 
			if(lastCatName != null)
			{
				sql = "SELECT name, id FROM categories WHERE id !=" + lastCatId;				
			}
			else
			{
				sql = "SELECT name, id FROM categories";
			}
			ResultSet result = stmt.executeQuery(sql);
			while(result.next())
			{
				String catName = result.getString("name"); 
				int catId = result.getInt("id"); 
		%>
				<option value="<%=catId%>"> <%out.print(catName);%> </option>
		<%
			}
		%>
	</select>
	<br/><input type="submit" name="action" id="action" value="Run Query">
		 <input type="submit" name="action" id="action" value="Reset">
</form>   

<br><br>

<table border="1" style="width:100%">

<%

    if(request.getParameter("action") == null || !request.getParameter("action").equals("Run Query"))
    {
    	
    }
    else 
    {
	PreparedStatement pstmt1 = null; 
	
	if(filter == null || filter.equals("all"))
	{
		if(order.equals("alphabetical"))
		{
			if(nextProduct == null)
			{
				pstmt1 = conn.prepareStatement("SELECT id,name FROM products ORDER BY name ASC");
			}
			else
			{
				pstmt1 = conn.prepareStatement("SELECT id,name FROM products WHERE name > '"+nextProduct+"' ORDER BY name ASC");				
			}
		}
		else
		{
			if(nextK == 0.0)
			{
			String test = "SELECT product_id, SUM(price) AS total FROM orders GROUP BY product_id ORDER BY SUM(price) DESC LIMIT 20";
			String test2 = "SELECT p.id AS id, p.name AS name, items.total AS price FROM products p,("+test+") as items WHERE items.product_id = p.id GROUP BY p.name, items.total, id ORDER BY items.total DESC LIMIT 20";
			pstmt1 = conn.prepareStatement(test2);
			}
			else
			{
				String test = "SELECT product_id, SUM(price) AS total FROM orders GROUP BY product_id ORDER BY SUM(price) < "+nextK+" DESC LIMIT 20";
				String test2 = "SELECT p.id AS id, p.name AS name, items.total AS price FROM products p,("+test+") as items WHERE items.product_id = p.id GROUP BY p.name, items.total, id ORDER BY items.total DESC LIMIT 20";
				pstmt1 = conn.prepareStatement(test2);				
			}
		}
	}
	else
	{
		if(order.equals("alphabetical"))
		{
		//	pstmt1 = conn.prepareStatement("SELECT id,name FROM products WHERE category_id = " + filter + " ORDER BY name ASC");
			
			if(nextProduct == null)
			{
				pstmt1 = conn.prepareStatement("SELECT id,name FROM products WHERE category_id = " + filter + " ORDER BY name ASC");
			}
			else
			{
				pstmt1 = conn.prepareStatement("SELECT id,name FROM products WHERE category_id = " + filter + " AND name > '"+nextProduct+"' ORDER BY name ASC");	
			}
		}
		else
		{			
			if(nextK == 0.0)
			{
				String test = "SELECT id, name FROM products WHERE category_id = " + filter + " GROUP BY name, id";
				String test2 = "SELECT product_id, SUM(price) AS total FROM orders GROUP BY product_id ORDER BY SUM(price) DESC";
				String test3 = "SELECT p.name AS name, items.total AS price, p.id AS id FROM ("+test+") AS p,("+test2+") as items WHERE items.product_id = p.id GROUP BY p.name, items.total, p.id LIMIT 20";			
				pstmt1 = conn.prepareStatement(test3); 
			}
			else
			{				
				String test = "SELECT id, name FROM products WHERE category_id = " + filter + " GROUP BY name, id";
				String test2 = "SELECT product_id, SUM(price) AS total FROM orders GROUP BY product_id ORDER BY SUM(price) < "+nextK+" DESC";
				String test3 = "SELECT p.name AS name, items.total AS price, p.id AS id FROM ("+test+") AS p,("+test2+") as items WHERE items.product_id = p.id GROUP BY p.name, items.total, p.id LIMIT 20";			
				pstmt1 = conn.prepareStatement(test3); 
			}
		}
	}
	
    ResultSet ts = pstmt1.executeQuery();

%>
    <tr>
    <td></td>
<%
    int productNum = 0; 
    while(ts.next() && productNum++ < 10)
    {
    	colLimits++; 

		session.setAttribute("alphabetical", ts.getString("name")); 
%>
	<td> <%out.println(ts.getString("name"));%></td>
<%
    }
%>
	</tr>
<% 
if(rows.equals("customers")){
	PreparedStatement pstmt2 = null;
	if(order.equals("alphabetical"))
	{
		if(nextName == null)
		{
			pstmt2 = conn.prepareStatement("SELECT id,name FROM users ORDER BY name ASC");	
		}
		else
		{
			pstmt2 = conn.prepareStatement("SELECT id,name FROM users ORDER BY name > '"+nextName+"' ASC");	
		}
	}
	else
	{
		String test = "SELECT user_id as id, SUM(price) as total FROM orders GROUP BY user_id ORDER BY SUM(price) DESC"; 
		String test1 = "SELECT u.id ,u.name AS name, SUM(herbert.total) AS price FROM users u, ("+test+") AS herbert WHERE herbert.id = u.id GROUP BY u.id ORDER BY SUM(herbert.total) DESC LIMIT 20"; 
		pstmt2 = conn.prepareStatement(test1); 
	}
	ResultSet rs = pstmt2.executeQuery();
	int rowsNumber = 0; 
	while(rs.next() && rowsNumber++ <20)
	{
		int counter=0;
		rowsLimits++; 
%>
	<tr>
	<td><%out.println(rs.getString("name"));%></td>
<%
		if(filter == null || filter.equals("all"))
		{	
			if(order.equals("alphabetical"))
			{
				pstmt2 = conn.prepareStatement("SELECT id,name FROM products ORDER BY name ASC");
			}
			else
			{
				String test = "SELECT product_id, SUM(price) AS total FROM orders GROUP BY product_id ORDER BY SUM(price) DESC LIMIT 20";
				String test2 = "SELECT p.id AS id, p.name AS name, items.total AS price FROM products p,("+test+") as items WHERE items.product_id = p.id GROUP BY p.id, items.total ORDER BY SUM(items.total) DESC LIMIT 20";
				pstmt2 = conn.prepareStatement(test2);
			}
		}
		else
		{			
			if(order.equals("alphabetical"))
			{
				pstmt2 = conn.prepareStatement("SELECT id,name FROM products WHERE category_id = " + filter + " ORDER BY name ASC");
			}
			else
			{
				String test = "SELECT id, name FROM products WHERE category_id = " + filter + " GROUP BY name, id";
				String test2 = "SELECT product_id, SUM(price) AS total FROM orders GROUP BY product_id ORDER BY SUM(price) DESC";
				String test3 = "SELECT p.id AS id, p.name AS name, items.total AS price FROM ("+test+") AS p,("+test2+") as items WHERE items.product_id = p.id GROUP BY name, p.id, items.total ORDER BY items.total DESC LIMIT 20";			
				pstmt2 = conn.prepareStatement(test3); 
			}
		}
		ResultSet rs2 = pstmt2.executeQuery();
		while(rs2.next()&& counter++ < 10){
			pstmt2 = conn.prepareStatement("SELECT SUM(price) AS tempprice FROM orders WHERE user_id = ? AND product_id = ? GROUP BY user_id");
			pstmt2.setInt(1,rs.getInt(1));
			pstmt2.setInt(2,rs2.getInt(1));
			ResultSet rs3 = pstmt2.executeQuery();
			if(rs3.next()){

				session.setAttribute("topk", "" + rs3.getFloat("tempprice"));
%>
				<td><%out.println("$" + rs3.getFloat("tempprice"));%></td>
<%	
			}
		}
%>
	</tr>
<%
	}
	}

    else if(rows.equals("states")){
    	System.out.println("in state if statement");
    	PreparedStatement pstmt2 = null; 
    	PreparedStatement pstmtTemp = null; 
    	int rest = 0; 
    	
    	if(order.equals("alphabetical"))
    	{
    		if(nextName == null)
    		{
    			pstmt2 = conn.prepareStatement("SELECT id,state FROM states ORDER BY state ASC LIMIT 20");
    		}
    		else
    		{
    			pstmt2 = conn.prepareStatement("SELECT id,state FROM states WHERE state > '"+nextName+"' ORDER BY state ASC LIMIT 20");
    		}
    	}
    	else 
    	{
    		String test = "SELECT state FROM states";
    		String test2 = "SELECT s.state AS pstate FROM states s WHERE s.state NOT IN (SELECT state FROM users) LIMIT 20";
    		String test1 = "SELECT SUM(o.price) AS sumPrice, s.state AS state FROM ("+test+") AS s , orders o,users u WHERE u.state = s.state GROUP BY s.state ORDER BY sumPrice DESC LIMIT 20";
    		pstmt2 = conn.prepareStatement(test1);
    		pstmtTemp = conn.prepareStatement(test2); 
    		ResultSet rsss = pstmt2.executeQuery();

    		if(nextSum == 1.1)
    		{
    			String test100 = "SELECT ss.state AS state From states ss WHERE ss.state NOT IN (SELECT state FROM users) AND ss.state NOT IN ("+test2+")";
    			pstmt2 = conn.prepareStatement(test100); 
    		}
    		if(request.getParameter("action") != null && request.getParameter("action").equals("Next 20 States"))
    		{
    			session.setAttribute("nextSum", "1.1"); 
    		}
    	} 
    	ResultSet rs = pstmt2.executeQuery();
    	
    	int rowsNumber = 0; 
    	while(rs.next() && rowsNumber++ <51)
    	{
    		rowsLimits++; 
    		
    		int counter=0;
    		
    		session.setAttribute("nextName", rs.getString("state")); 
    %>
    	<tr>
    	<td><%out.println(rs.getString("state"));%></td>
    <%
    		if(filter == null || filter.equals("all"))
    		{
    			if(order.equals("alphabetical"))
    			{
    				pstmt2 = conn.prepareStatement("SELECT id,name FROM products ORDER BY name ASC");
    			}
    			else
    			{
    				String test = "SELECT product_id, SUM(price) AS total FROM orders GROUP BY product_id ORDER BY SUM(price) DESC LIMIT 20";
    				String test2 = "SELECT p.id AS id, p.name AS name, items.total AS price FROM products p,("+test+") as items WHERE items.product_id = p.id GROUP BY p.id, items.total ORDER BY SUM(items.total) DESC LIMIT 20";
    				pstmt2 = conn.prepareStatement(test2);
    			}
    		}
    		else
    		{
    			//pstmt2 = conn.prepareStatement("SELECT id,name FROM products WHERE category_id = " + filter + " ORDER BY name ASC");
    			
    			if(order.equals("alphabetical"))
    			{
    				pstmt2 = conn.prepareStatement("SELECT id,name FROM products WHERE category_id = " + filter + " ORDER BY name ASC");
    			}
    			else
    			{
    				String test = "SELECT id, name FROM products WHERE category_id = " + filter + " GROUP BY name, id";
    				String test2 = "SELECT product_id, SUM(price) AS total FROM orders GROUP BY product_id ORDER BY SUM(price) DESC";
    				String test3 = "SELECT p.id AS id, p.name AS name, items.total AS price FROM ("+test+") AS p,("+test2+") as items WHERE items.product_id = p.id GROUP BY name, p.id, items.total ORDER BY items.total DESC LIMIT 20";			
    				pstmt2 = conn.prepareStatement(test3); 
    			}
    		}
    		ResultSet rs2 = pstmt2.executeQuery();
    		while(rs2.next()&& counter++ < 10){
    			pstmt2 = conn.prepareStatement("SELECT SUM(o.price) AS sumPrice FROM orders o,users u WHERE o.product_id = ? AND o.user_id = u.id AND u.state = ? ");
    			pstmt2.setInt(1,rs2.getInt(1));
    			pstmt2.setString(2,rs.getString("state"));
    			ResultSet rs3 = pstmt2.executeQuery();
    			if(rs3.next()){

    				session.setAttribute("topk", "" + rs3.getFloat("sumPrice"));

    %>
    				<td><%out.println("$" + rs3.getFloat("sumPrice"));%></td>
    <%	
    			}
    		}
    %>
    	</tr>
    <%
    	}
    	if(pstmtTemp != null)
    	{
    		ResultSet rstp = pstmtTemp.executeQuery(); 
    		while(rstp.next() && 20-(rowsNumber++) > 0)
    		{
    			rowsLimits++; 
    %>
    		<tr>
    		<td><%out.println(rstp.getString("pstate"));%></td>
   	<% 
   			int empty = 0;
   			while(empty++ < 10)
   			{
   	%>
   				<td><%out.println("$0.0");%></td>
   	<%
   			}
   	%>
    		</tr>
    <% 
    		}
    	}
    }
    }
%>
</table>
<form>
<%
	String next = null; 
	if(rows != null && rows.equals("customers") && rowsLimits >= 20)
	{
		next = "Next 20 Customers"; 
	}
	else if(rows != null && rows.equals("states") && rowsLimits >= 20)
	{
		next = "Next 20 States"; 
	}
	if(next != null)
	{
%>
		<br><input type="submit" name="action" id="action" value="<%=next%>">
			 <input type="submit" name="action" id="action" value="Next 10 Products">
<%
	}
	else if (next == null && colLimits >= 10)
	{
%>
		<br><input type="submit" name="action" id="action" value="Next 10 Products">	
<%		
	}
%>
</form>
</body>
</html>